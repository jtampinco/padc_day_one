package androidtutorials.qc47east.ph.a47east_projectone;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by jtampinco on 4/16/2016.
 */
public class MainActivityOrganizer implements View.OnClickListener{

    private ImageView iv_image_one;
    private ImageView iv_image_two;
    private ImageView iv_image_three;
    private ImageView iv_image_four;
    private ImageView iv_image_five;
    private ImageView iv_image_six;
    private ImageView iv_image_seven;
    private ImageView iv_image_eight;
    private ImageView iv_image_nine;
    private ImageView iv_image_ten;
    private ImageView iv_image_eleven;
    private ImageView iv_image_twelve;
    private ImageView iv_image_thirteen;
    private ImageView iv_image_fourteen;
    private ImageView iv_image_fifteen;
    private ImageView iv_image_sixteen;

    private ArrayList<ImageView> iv_container;
    private ArrayList<Drawable> iv_drawables;
    private ArrayList<Integer> iv_sequence;

    private int click_count = 0;
    private View previously_clicked_image = null;
    private int max_click = 2;

    public MainActivityOrganizer(){
        iv_container = new ArrayList<>();
        iv_drawables = new ArrayList<>();
        iv_sequence = new ArrayList<>();
    }

    public void organize(){

    }

    @Override
    public void onClick(View v) {

    }
}
