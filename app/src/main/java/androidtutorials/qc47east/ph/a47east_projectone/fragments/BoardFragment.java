package androidtutorials.qc47east.ph.a47east_projectone.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import androidtutorials.qc47east.ph.a47east_projectone.Card;
import androidtutorials.qc47east.ph.a47east_projectone.R;

/**
 * Created by jtampinco on 4/29/2016.
 */
public class BoardFragment extends Fragment {

    ArrayList<Drawable> resourceList;
    ArrayList<Integer> sequenceList;
    HashMap<Integer, Card> cardMap;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) inflater.inflate(R.layout.recycler_view, container, false);
        ContentAdapter contentAdapter = new ContentAdapter();
        recyclerView.setAdapter(contentAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        generateResources();
        return recyclerView;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(View view){
            super(view);
        }
    }

    public class ContentAdapter extends RecyclerView.Adapter<ViewHolder>{
        private static final int LENGTH = 16;
        private int counter = 0;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            int iteration = counter++;

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, parent, false);
            ImageButton image = (ImageButton) view.findViewById(R.id.imageButton);
            image.setImageDrawable(resourceList.get(sequenceList.get(iteration)));
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Snackbar.make(v, "Image Clicked", Snackbar.LENGTH_SHORT).show();
                }
            });

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return LENGTH;
        }
    }

    public void generateResources(){
        cardMap = new HashMap<>();
        resourceList = new ArrayList<>();
        sequenceList = new ArrayList<>();

        // Insert drawables in ArrayList
        ContextCompat compat = new ContextCompat();
        resourceList.add(compat.getDrawable(getActivity(), R.drawable.image_one));
        resourceList.add(compat.getDrawable(getActivity(), R.drawable.image_two));
        resourceList.add(compat.getDrawable(getActivity(), R.drawable.image_three));
        resourceList.add(compat.getDrawable(getActivity(), R.drawable.image_four));
        resourceList.add(compat.getDrawable(getActivity(), R.drawable.image_five));
        resourceList.add(compat.getDrawable(getActivity(), R.drawable.image_six));
        resourceList.add(compat.getDrawable(getActivity(), R.drawable.image_seven));
        resourceList.add(compat.getDrawable(getActivity(), R.drawable.image_eight));

        // Generate random sequence
        for(int a = 0; a < 2; a++){
            for(int b = 0; b < 8; b++){
                sequenceList.add(b);
            }
        }
        Collections.shuffle(sequenceList);
    }

//    @Override
//    public void onClick(View v) {
//        // Save the clicked view for future use
//        ImageView clickedImageView = (ImageView) v;
//
//        // Animate the click event
//        v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.anim_scale));
//
//        // On the first click event
//        if (onFirstClick) {
//            // Save the first image
//            savedImageView = clickedImageView;
//            onFirstClick = false;
//        } else {
//            // If the clicked image is not the same
//            if (savedImageView.getId() != clickedImageView.getId()) {
//                // Compare the two images
//                if (savedImageView.getDrawable().getConstantState().equals(clickedImageView.getDrawable().getConstantState())) {
//
//                    // If the two images are the same, make them disappear
//                    savedImageView.setImageDrawable(null);
//                    clickedImageView.setImageDrawable(null);
//
//                    // Remove the click events of the view
//                    savedImageView.setOnClickListener(null);
//                    clickedImageView.setOnClickListener(null);
//
//                    // Add a point to the score
//                    int score = Integer.parseInt(tv_score.getText().toString()) + 1;
//                    tv_score.setText(score);
//                }
//                onFirstClick = true;
//            }
//        }
//    }
}
