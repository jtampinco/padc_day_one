package androidtutorials.qc47east.ph.a47east_projectone;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidtutorials.qc47east.ph.a47east_projectone.fragments.BoardFragment;

public class MainActivity extends AppCompatActivity{

    private Button btn_reset;
    private TextView tv_score;
    private ViewPager viewPager;

    private boolean onFirstClick = true;
    private ImageView savedImageView = null;

    private ArrayList<ImageView> iv_container;
    private ArrayList<Drawable> iv_drawables;
    private ArrayList<Integer> iv_sequence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_score = (TextView) findViewById(R.id.tv_score);
        viewPager = (ViewPager) findViewById(R.id.ViewPager);
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new BoardFragment(), "GAME 1");

        viewPager.setAdapter(adapter);

        btn_reset = (Button) findViewById(R.id.btn_reset);
        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetGame();
            }
        });

    }

    public class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    public void resetGame(){
//        attachListeners();
//        attachDrawables();
//        attachImageViewsToGrid();
    }


}