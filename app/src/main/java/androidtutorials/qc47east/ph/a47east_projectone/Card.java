package androidtutorials.qc47east.ph.a47east_projectone;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.widget.ImageButton;

/**
 * Created by jtampinco on 4/30/2016.
 */

public class Card {

    private int _id;
    private ImageButton view;

    private Drawable image;
    private Drawable cardBack;
    private Drawable cardFinished;

    private boolean isFlipped;

    public Card(int _id, ImageButton view, Drawable image, Context context){
        this._id = _id;
        this.view = view;

        this.image = image;
        cardBack = new ContextCompat().getDrawable(context, R.drawable.des_border_box2);
        cardFinished = new ContextCompat().getDrawable(context, R.drawable.des_border_box);

        isFlipped = false;
    }

    public void showImage(){
        view.setImageDrawable(image);
        isFlipped = true;
    }

    public void showBack(){
        view.setImageDrawable(cardBack);
        isFlipped = false;
    }

    public Drawable getImage(){
        return image;
    }

    public boolean isCardFlipped(){
        return isFlipped;
    }

    public void cardMatched(){
        view.setImageDrawable(cardFinished);
        isFlipped = true;
    }

    public boolean isMatch(Card card){
        if (card.getImage().getConstantState().equals(image.getConstantState())) {
            cardMatched();
            card.cardMatched();
            return true;
        }

        showBack();
        card.showBack();
        return false;
    }


}
